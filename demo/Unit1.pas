unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls, FSDialog;

type
  TForm1 = class(TForm)
    FSDialog1: TFSDialog;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Memo1: TMemo;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
  FSDialog1.ShowModal('Teste');
  if FSDialog1.ShowModal('Teste', dtSimNao) = mrOk then
    ShowMessage('Sim');
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  MessageDlg('Teste', mtWarning, mbYesNo, 0, mbYes);
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  FSDialog1.ShowModal('Teste')
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  FSDialog1.Show('Vamos');
  Sleep(4000);
  FSDialog1.Close;
end;

procedure TForm1.Button5Click(Sender: TObject);
begin
  FSDialog1.ShowWithTime('Msg', 5);
end;

end.
