unit FSDialog;

interface

uses
  System.SysUtils, System.Classes, VCL.Controls, VCL.Forms, UFSFrmDialog,
  UFSFrmDialogSimNao;

type
  TFSDialogType = (dtOk, dtSimNao);
  TFSDefaultButton = (dbNenhum, dbSim, dbNao);

  TFSDialog = class(TComponent)
  private
    { Private declarations }
    FForm: TFSFrmDialog;
  protected
    { Protected declarations }
  public
    { Public declarations }
    function ShowModal(Mensagem: String): TModalResult; overload;
    function ShowModal(Mensagem: String; DialogType: TFSDialogType)
      : TModalResult; overload;
    function ShowModal(Mensagem: String; DialogType: TFSDialogType;
      Caption: String): TModalResult; overload;
    function ShowModal(Mensagem: String; DialogType: TFSDialogType;
      Caption: String; DefaultButton: TFSDefaultButton): TModalResult; overload;

    procedure ShowWithTime(Mensagem: String; Tempo: Integer);
    procedure Show(Mensagem: String);
    procedure Close();
  published
    { Published declarations }
  end;

  TExibeFormThread = class(TThread)
  private
    FTempo: Integer;
    FMensagem: String;
  public
    constructor Create(Mensagem: String; Tempo: Integer); overload;
  protected
    procedure Execute; override;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('FS', [TFSDialog]);
end;

{ TFSDialog }

procedure TFSDialog.Close;
begin
  if Assigned(FForm) then
  begin
    FForm.Close;
    FreeAndNil(FForm);
  end;
end;

procedure TFSDialog.Show(Mensagem: String);
begin
  if not Assigned(FForm) then
  begin
    FForm := TFSFrmDialog.Create(nil);
  end;
  FForm.btn01.Visible := False;
  FForm.lblMensagem.Caption := Mensagem;
  FForm.Show;
  Application.ProcessMessages;
  FForm.BringToFront;
end;

function TFSDialog.ShowModal(Mensagem: String; DialogType: TFSDialogType;
  Caption: String; DefaultButton: TFSDefaultButton): TModalResult;
var
  FFormLocal: TFSFrmDialog;
begin
  case DialogType of
    dtOk:
      begin
        FFormLocal := TFSFrmDialog.Create(nil);
      end;
    dtSimNao:
      begin
        FFormLocal := TFSFrmDialogSimNao.Create(nil);
        case DefaultButton of
          dbSim:
            begin
              (FFormLocal as TFSFrmDialogSimNao).btn01.SetFocus;
            end;
          dbNao:
            begin
              (FFormLocal as TFSFrmDialogSimNao).btn02.SetFocus;
            end;
        end;
      end;
  end;
  FFormLocal.lblMensagem.Caption := Mensagem;
  Result := FFormLocal.ShowModal;
  FreeAndNil(FFormLocal);
end;

procedure TFSDialog.ShowWithTime(Mensagem: String; Tempo: Integer);
var
  ExibeFormThread: TExibeFormThread;
begin
  ExibeFormThread := TExibeFormThread.Create(Mensagem, Tempo);
  ExibeFormThread.Start;
end;

function TFSDialog.ShowModal(Mensagem: String): TModalResult;
begin
  ShowModal(Mensagem, dtOk, '', dbNenhum);
end;

function TFSDialog.ShowModal(Mensagem: String; DialogType: TFSDialogType)
  : TModalResult;
begin
  ShowModal(Mensagem, DialogType, '', dbNenhum);
end;

function TFSDialog.ShowModal(Mensagem: String; DialogType: TFSDialogType;
  Caption: String): TModalResult;
begin
  ShowModal(Mensagem, DialogType, Caption, dbNenhum);
end;

{ TExibeFormThread }

constructor TExibeFormThread.Create(Mensagem: String; Tempo: Integer);
begin
  inherited Create(True);
  FreeOnTerminate := True;
  Priority := tpNormal;
  FMensagem := Mensagem;
  FTempo := Tempo;
end;

procedure TExibeFormThread.Execute;
var
  FForm: TFSFrmDialog;
begin
  inherited;
  try
    FForm := TFSFrmDialog.Create(nil);
    FForm.btn01.Visible := False;
    FForm.lblMensagem.Caption := FMensagem;
    FForm.Show;
    Application.ProcessMessages;
    FForm.BringToFront;
    Sleep(FTempo * 1000);
  finally
    FForm.Close;
    FreeAndNil(FForm);
  end;
end;

end.
